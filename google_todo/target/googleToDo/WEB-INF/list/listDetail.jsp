<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>List ToDo</title>
</head>
<body>
<h3>To Do</h3>
<table border="1">
	<form action = "${pageContext.request.contextPath}/task/update?id=${toShow.id}" method="post">
		<fieldset>
		Title:${toShow.title}
		<c:forEach var="task" items="${tasks}">
			<tr>
				<td>${task.id}</td>
				<td>${task.title}</td>
				<c:forEach var="operation" items="${operations.entrySet()}">
					<td><a href="${pageContext.request.contextPath}/task/${operation.key}?id=${task.id}&idList=${task.idList}">${operation.value}</a></td>
				</c:forEach>
			</tr>
		</c:forEach>
		</fieldset>
	</form>
	<form action = "${pageContext.request.contextPath}/task/create?idList=${toShow.id}" method="post">
		Title:
		<input type="text" name="titleIn">
		<input type="submit" value="Create">
	</form>
</table>
</body>
</html>