<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>List ToDo</title>
</head>
<body>
<h3>To Do</h3>
<table border="1">
	<caption>ToDo</caption>
	<tr>
		<td>Id</td>
		<td>Name</td>
	</tr>
	<c:forEach var="list" items="${lists}">
		<tr>
			<td>${list.id}</td>
			<td>${list.title}</td>
			<c:forEach var="operation" items="${operations.entrySet()}">
				<td><a href="${pageContext.request.contextPath}/list/${operation.key}?id=${list.id}">${operation.value}</a></td>
			</c:forEach>
		</tr>
	</c:forEach>
	
	<form action = "${pageContext.request.contextPath}/list/create" method="post">
		Title:
		<input type="text" name="titleIn">
		<input type="submit" value="Create">
	</form>
</table>
</body>
</html>