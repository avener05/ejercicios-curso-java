package mx.com.praxis.services.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;

import mx.com.praxis.daos.ListDao;
import mx.com.praxis.daos.TaskDao;
import mx.com.praxis.dtos.ListDto;
import mx.com.praxis.services.ListService;
import mx.com.praxis.utils.Converter;

@Stateless
@TransactionManagement(value = TransactionManagementType.CONTAINER)
public class ListServiceImpl implements ListService{
	
	@Inject
	private ListDao listDao;

	@Inject
	private TaskDao taskDao;

	public List<ListDto> getAllListDto() {
		return Converter.convertListsToListDtos(listDao.findAll());
	}

	@Override
	@TransactionAttribute(value =TransactionAttributeType.REQUIRED)
	public void create(ListDto listDto) {
		listDao.create(Converter.convertListDtoToList(listDto));	
	}

	@Override
	public ListDto getListDtoById(Long id) {
		return Converter.convertListToListDto(
				listDao.findById(id));
	}

	@Override
	public void update(ListDto toUpdate) {
		listDao.update(Converter.convertListDtoToListUpdate(toUpdate));
		
	}

	@Override
	public void delete(Long id) {
		taskDao.findAll(id)
		.forEach(taskDao::delete);
		
		listDao.delete(listDao.findById(id));
	}

}
