package mx.com.praxis.services;

import java.util.List;

import javax.ejb.Local;

import mx.com.praxis.dtos.TaskDto;

@Local
public interface TaskService {
	
	List<TaskDto> getAllTaskDto(Long id);
	
	void create(TaskDto toCreate);
	
	TaskDto getTaskDtoById(Long id);
	
	void update(TaskDto toUpdate);

	void delete(Long id);
}
