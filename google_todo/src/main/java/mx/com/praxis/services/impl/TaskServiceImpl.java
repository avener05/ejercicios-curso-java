package mx.com.praxis.services.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.inject.Inject;

import mx.com.praxis.daos.TaskDao;
import mx.com.praxis.dtos.TaskDto;
import mx.com.praxis.services.TaskService;
import mx.com.praxis.utils.Converter;

@Stateless
@TransactionManagement(value = TransactionManagementType.CONTAINER)
public class TaskServiceImpl implements TaskService{
	
	@Inject
	private TaskDao taskDao;

	public List<TaskDto> getAllTaskDto(Long id) {
		return Converter.convertTasksToTaskDtos(taskDao.findAll(id));
	}

	@Override
	@TransactionAttribute(value =TransactionAttributeType.REQUIRED)
	public void create(TaskDto taskDto) {
		taskDao.create(Converter.convertTaskDtoToTask(taskDto));	
	}

	@Override
	public TaskDto getTaskDtoById(Long id) {
		return Converter.convertTaskToTaskDto(
				taskDao.findById(id));
	}

	@Override
	public void update(TaskDto toUpdate) {
		taskDao.update(Converter.convertTaskDtoToTaskUpdate(toUpdate));
		
	}

	@Override
	public void delete(Long id) {
		taskDao.delete(taskDao.findById(id));
	}

}
