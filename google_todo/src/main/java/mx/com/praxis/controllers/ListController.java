package mx.com.praxis.controllers;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.praxis.dtos.ListDto;
import mx.com.praxis.services.ListService;
import mx.com.praxis.services.TaskService;
import mx.com.praxis.utils.Converter;

@WebServlet(urlPatterns = {
		"/list/create",
		"/list/update",
		"/list/read",
		"/list/delete"
})
public class ListController extends HttpServlet{
	
	private static final Map<String,String> OP = new HashMap<>(3);
	
	static {
		OP.put("read", "READ");
		OP.put("update", "UPDATE");
		OP.put("delete", "DELETE");
	}
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Inject
	private ListService listService;
	
	@Inject
	private TaskService taskService;
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String operation = request.getServletPath();
		
		if("/list/delete".equals(operation)) {
			deleteList(request,response);
		}
		
		if("/list/update".equals(operation)) {
			updateListGet(request,response);
		}
		
		if("/list/read".equals(operation)) {
			readList(request,response);
		}
	}
	
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String operation = request.getServletPath();
		
		if("/list/create".equals(operation)) {
			createList(request,response);
		}
		
		if("/list/update".equals(operation)) {
			updateList(request,response);
		}
	}
	
	private void createList(HttpServletRequest request, HttpServletResponse response) throws IOException {
		ListDto toCreate = Converter.convertRequestToListDto(request);
		listService.create(toCreate);
		String url =request.getContextPath()+"/";
		response.sendRedirect(url);
	}
	
	private void updateList(HttpServletRequest request, HttpServletResponse response) throws IOException {
		ListDto toUpdate = Converter.convertRequestToListDtoUpdate(request);
		listService.update(toUpdate);
		
		String url =request.getContextPath()+"/";
		response.sendRedirect(url);
	}
	
	private void deleteList(HttpServletRequest request, HttpServletResponse response) throws IOException {
		Long id = Long.parseLong(request.getParameter("id"));
		listService.delete(id);
		
		String url =request.getContextPath()+"/";
		response.sendRedirect(url);
	}
	
	private void updateListGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		Long id = Long.parseLong(request.getParameter("id"));
		request.setAttribute("toUpdate", listService.getListDtoById(id));
		request.getRequestDispatcher("/WEB-INF/list/listUpdate.jsp").forward(request, response);
	}
	

	private void readList(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Long id = Long.parseLong(request.getParameter("id"));
		request.setAttribute("operations", OP);
		request.setAttribute("toShow", listService.getListDtoById(id));
		request.setAttribute("tasks", taskService.getAllTaskDto(id));
		request.getRequestDispatcher("/WEB-INF/list/listDetail.jsp").forward(request, response);
	}

}
