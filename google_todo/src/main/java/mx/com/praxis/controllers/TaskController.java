package mx.com.praxis.controllers;

import java.io.IOException;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mx.com.praxis.dtos.TaskDto;
import mx.com.praxis.services.TaskService;
import mx.com.praxis.utils.Converter;

@WebServlet(urlPatterns = {
		"/task/create",
		"/task/update",
		"/task/read",
		"/task/delete"
})
public class TaskController extends HttpServlet{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Inject
	private TaskService taskService;
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String operation = request.getServletPath();
		
		if("/task/delete".equals(operation)) {
			deleteList(request,response);
		}
		
		if("/task/update".equals(operation)) {
			updateListGet(request,response);
		}
		
		if("/task/read".equals(operation)) {
			updateListGet(request,response);
		}
	}
	
	@Override
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String operation = request.getServletPath();
		
		if("/task/create".equals(operation)) {
			createList(request,response);
		}
		
		if("/task/update".equals(operation)) {
			updateList(request,response);
		}
	}
	
	private void createList(HttpServletRequest request, HttpServletResponse response) throws IOException {
		TaskDto toCreate = Converter.convertRequestToTaskDto(request);
		taskService.create(toCreate);
		String url =request.getContextPath()+"/";
		response.sendRedirect(url);
	}
	
	private void updateList(HttpServletRequest request, HttpServletResponse response) throws IOException {
		TaskDto toUpdate = Converter.convertRequestToTaskDtoUpdate(request);
		taskService.update(toUpdate);
		
		String url =request.getContextPath()+"/";
		response.sendRedirect(url);
	}
	
	private void deleteList(HttpServletRequest request, HttpServletResponse response) throws IOException {
		Long id = Long.parseLong(request.getParameter("id"));
		taskService.delete(id);
		
		String url =request.getContextPath()+"/";
		response.sendRedirect(url);
	}
	
	private void updateListGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		Long id = Long.parseLong(request.getParameter("id"));
		request.setAttribute("toUpdate", taskService.getTaskDtoById(id));
		request.getRequestDispatcher("/WEB-INF/task/taskUpdate.jsp").forward(request, response);
	}
}
