package mx.com.praxis.daos;

import javax.ejb.Local;

import mx.com.praxis.entities.Task;

@Local
public interface TaskDao  extends GenericDao<Task,Long>{

}
