package mx.com.praxis.daos;

import javax.ejb.Local;

@Local
public interface ListDao  extends GenericDao<mx.com.praxis.entities.List,Long>{

}
