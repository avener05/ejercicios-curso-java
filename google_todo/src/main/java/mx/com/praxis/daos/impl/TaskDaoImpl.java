package mx.com.praxis.daos.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import mx.com.praxis.daos.TaskDao;
import mx.com.praxis.entities.Task;

@Stateless
public class TaskDaoImpl implements TaskDao{
	
	@PersistenceContext(name = "AccessDataBase")
	private EntityManager entityManager;
	
	

	public List<Task> findAll(Long id) {
		TypedQuery<Task> queryToFindAllList = entityManager.createNamedQuery("Task.findAll",Task.class)
				.setParameter("idList",id);
		return queryToFindAllList.getResultList();
	}

	public Task findById(Long id) {
		TypedQuery<Task> queryToFindById = entityManager.createNamedQuery("Task.findById",Task.class)
				.setParameter("id",id);
		return queryToFindById.getSingleResult();
	}

	public void create(Task toCreate) {
		entityManager.persist(toCreate);
	}

	public void update(Task toUpdate) {
		entityManager.merge(toUpdate);
	}

	public void delete(Task toDelete) {
		entityManager.remove(toDelete);
		
	}

	@Override
	public List<Task> findAll() {
		// TODO Auto-generated method stub
		return null;
	}
	
}
