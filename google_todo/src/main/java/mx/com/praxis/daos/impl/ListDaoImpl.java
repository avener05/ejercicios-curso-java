package mx.com.praxis.daos.impl;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import mx.com.praxis.daos.ListDao;

@Stateless
public class ListDaoImpl implements ListDao{
	
	@PersistenceContext(name = "AccessDataBase")
	private EntityManager entityManager;
	
	

	public List<mx.com.praxis.entities.List> findAll() {
		TypedQuery<mx.com.praxis.entities.List> queryToFindAllList = entityManager.createNamedQuery("List.findAll",mx.com.praxis.entities.List.class);
		return queryToFindAllList.getResultList();
	}

	public mx.com.praxis.entities.List findById(Long id) {
		TypedQuery<mx.com.praxis.entities.List> queryToFindById = entityManager.createNamedQuery("List.findById",mx.com.praxis.entities.List.class)
				.setParameter("id",id);
		return queryToFindById.getSingleResult();
	}

	public void create(mx.com.praxis.entities.List toCreate) {
		entityManager.persist(toCreate);
	}

	public void update(mx.com.praxis.entities.List toUpdate) {
		entityManager.merge(toUpdate);
	}

	public void delete(mx.com.praxis.entities.List toDelete) {
		entityManager.remove(toDelete);
		
	}

	@Override
	public List<mx.com.praxis.entities.List> findAll(Long id) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
