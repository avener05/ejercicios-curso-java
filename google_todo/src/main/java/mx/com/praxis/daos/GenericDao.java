package mx.com.praxis.daos;

import java.util.List;

import javax.ejb.Local;

public interface GenericDao <T,ID>{
	
	List<T> findAll();
	
	List<T> findAll(ID id);
	
	T findById(ID id);
	
	void create(T toCreate);
	
	void update(T toUpdate);
	
	void delete(T toDelete);
	

}
