package mx.com.praxis.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="task")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor(access=AccessLevel.PRIVATE)
@Builder
@NamedQueries({
	@NamedQuery(name = "Task.findAll",query = "select t from Task t where t.idList=:idList"),
	@NamedQuery(name = "Task.findById",query = "select t from Task t where t.id=:id")
})

public class Task implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "id_Sequence1")
	@SequenceGenerator(name = "id_Sequence1", sequenceName = "TASKS_SEQ")
	private Long id;
	
	@Column(name = "id_lists")
	private Long idList;
	
	@Column(name = "is_complete")
	private Long isComplete;
	
	@Column(length = 100)
	private String title;
}
