package mx.com.praxis.utils;

import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import mx.com.praxis.dtos.ListDto;
import mx.com.praxis.dtos.TaskDto;
import mx.com.praxis.entities.Task;

@NoArgsConstructor(access=AccessLevel.PRIVATE)
public class Converter {
	
	public static List<ListDto> convertListsToListDtos(List<mx.com.praxis.entities.List> source){
		return source
				.stream()
				.map(Converter::convertListToListDto)
				.collect(Collectors.toList());
		
	}
	
	public static ListDto convertListToListDto(mx.com.praxis.entities.List source){
		return ListDto.builder()
				.id(source.getId())
				.title(source.getTitle())
				.build();
	}
	
	public static mx.com.praxis.entities.List convertListDtoToList(ListDto listDto){
		return mx.com.praxis.entities.List.builder()
				.title(listDto.getTitle())
				.build();
		
	}
	
	public static mx.com.praxis.entities.List convertListDtoToListUpdate(ListDto listDto){
		return mx.com.praxis.entities.List.builder()
				.id(listDto.getId())
				.title(listDto.getTitle())
				.build();
		
	}
	
	public static ListDto convertRequestToListDto(HttpServletRequest source){
		return ListDto.builder()
				.title(source.getParameter("titleIn"))
				.build();
	}
	
	public static ListDto convertRequestToListDtoUpdate(HttpServletRequest source){
		return ListDto.builder()
				.id(Long.parseLong(source.getParameter("id")))
				.title(source.getParameter("titleUpdate"))
				.build();
	}
	
	public static List<TaskDto> convertTasksToTaskDtos(List<Task> source){
		return source
				.stream()
				.map(Converter::convertTaskToTaskDto)
				.collect(Collectors.toList());
		
	}
	
	public static TaskDto convertTaskToTaskDto(Task source){
		return TaskDto.builder()
				.id(source.getId())
				.idList(source.getIdList())
				.title(source.getTitle())
				.build();
	}
	
	public static Task convertTaskDtoToTask(TaskDto taskDto){
		return Task.builder()
				.idList(taskDto.getIdList())
				.title(taskDto.getTitle())
				.build();
		
	}
	
	public static Task convertTaskDtoToTaskUpdate(TaskDto taskDto){
		return Task.builder()
				.id(taskDto.getId())
				.idList(taskDto.getIdList())
				.title(taskDto.getTitle())
				.build();
		
	}
	
	public static TaskDto convertRequestToTaskDto(HttpServletRequest source){
		return TaskDto.builder()
				.idList(Long.parseLong(source.getParameter("idList")))
				.title(source.getParameter("titleIn"))
				.build();
	}
	
	public static TaskDto convertRequestToTaskDtoUpdate(HttpServletRequest source){
		return TaskDto.builder()
				.id(Long.parseLong(source.getParameter("id")))
				.idList(Long.parseLong(source.getParameter("idList")))
				.title(source.getParameter("titleUpdate"))
				.build();
	}
}
