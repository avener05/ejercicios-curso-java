package com.finaltest.config;

import java.sql.SQLException;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ExampleJdbc {

	private static final Logger log = LogManager.getLogger();
	private static JdbcConfig config = null;
	
	public void nuevoJdbc() throws SQLException {
		log.trace("JDBC Trace");
		log.debug("JDBC Debug");
		log.info("JDBC Info");
		log.warn("JDBC Warn");
		log.error("JDBC Error");
		log.fatal("JDBC Fatal");
		
		log.log(Level.getLevel("MYLOG"),"Test MYLOG");
		Level codeLevel = Level.forName("CODELEVEL", 500);
		log.log(codeLevel, "code level");
		config.conectar();
	}
}
