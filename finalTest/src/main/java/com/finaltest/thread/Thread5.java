package com.finaltest.thread;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;

import com.finaltest.levels.impl.LogLevels;

public class Thread5 extends Thread{
	private static final Logger log = LogManager.getLogger();

	@Override
	public void run() {
		Runnable r1 = () -> {
			ThreadContext.push("name", "Angel Chavez");
			ThreadContext.push("userName", "Ron");
			log.fatal("Test fatal");
		};
		Thread t1 = new Thread(r1);
		t1.start();
	}

}
