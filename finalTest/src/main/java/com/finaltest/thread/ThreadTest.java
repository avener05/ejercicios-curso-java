package com.finaltest.thread;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ThreadTest implements Runnable{
	private static final Logger log = LogManager.getLogger();
	
	public ThreadTest() {
		super();
		this.run();
	}



	@Override
	public void run() {
		Thread1 t1 = new Thread1();
		t1.run();
		Thread2 t2 = new Thread2();
		t2.run();
		Thread3 t3 = new Thread3();
		t3.run();
		Thread4 t4 = new Thread4();
		t4.run();
		Thread5 t5 = new Thread5();
		t5.run();
		Thread6 t6 = new Thread6();
		t6.run();
		Thread7 t7 = new Thread7();
		t7.run();
		Thread8 t8 = new Thread8();
		t8.run();
		Thread9 t9 = new Thread9();
		t9.run();
		Thread10 t10 = new Thread10();
		t10.run();
		Thread11 t11 = new Thread11();
		t11.run();
		Thread12 t12 = new Thread12();
		t12.run();
	}
}
