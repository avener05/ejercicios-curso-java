package com.finaltest.thread;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;

import com.finaltest.levels.impl.LogLevels;

public class Thread3 extends Thread{
	private static final Logger log = LogManager.getLogger();

	@Override
	public void run() {
		Runnable r1 = () -> {
			ThreadContext.push("name", "Angel Chavez");
			ThreadContext.push("userName", "Hagrid");
			log.debug("Test debug");
		};
		Thread t1 = new Thread(r1);
		t1.start();
	}

}
