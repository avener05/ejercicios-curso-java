<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ page import="java.util.*" %>
<%@ page import="AngelWeb.controller.Automovil" %>

<html>
   <head>
      <title>Lista de automoviles</title>
   </head>

   <body>
		<% 
		Set<Automovil> list = (Set<Automovil>) request.getAttribute("Lista");
		for(Automovil auto : list){
		%>
		Automovil: <%=auto.getModelo()%>
		<br/>
		Placa: <%=auto.getPlaca()%>
		<br/>
		Precio: <%=auto.getPrecio()%>
		<br/>
		<br/>
      	<%
		}
      	%>
   </body>
</html>