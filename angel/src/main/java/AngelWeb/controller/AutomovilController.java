package AngelWeb.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.util.HashSet;
import java.util.Set;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AutomovilController extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	   private String message;
	   private AutomovilDAOImpl DATA;

	   public void init() throws ServletException {
		   Automovil a1 = new Automovil("Golf","GHD-A67",20000);
		   Automovil a2 = new Automovil("Jetta","ADD-A64",40000);
		   Set<Automovil> aux = new HashSet<>();
		   aux.add(a1);
		   aux.add(a2);
		   DATA=new AutomovilDAOImpl(aux);
	   }

	   public void doGet(HttpServletRequest request, HttpServletResponse response)
	      throws ServletException, IOException {
		   Set<Automovil> list = DATA.getAll();
		   request.setAttribute("Lista", list);
		   RequestDispatcher requestDispatcher = request
			        .getRequestDispatcher("/showSet.jsp");
		   requestDispatcher.forward(request, response);
	   }
	   
	   public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
		   DATA.addAutomovil(new Automovil(request.getParameter("model"), 
				   request.getParameter("id"), 
				   Integer.valueOf(request.getParameter("price"))));
		   RequestDispatcher requestDispatcher = request
			        .getRequestDispatcher("/index2.jsp");
		   requestDispatcher.forward(request, response);
	   }

	   public void destroy() {
	      // do nothing.
	   }

}
