package AngelWeb.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class AutomovilControllerinicial extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	   private String message;

	   public void init() throws ServletException {
	      // Do required initialization
	      message = "HOLA MUNDO";
	   }

	   public void doGet(HttpServletRequest request, HttpServletResponse response)
	      throws ServletException, IOException {
	      
	      // Set response content type
	      response.setContentType("text/html");

	      // Actual logic goes here.
	      PrintWriter out = response.getWriter();
	      out.println("<h1>" + message + "</h1>");
	   }
	   
//	   public void doPost(HttpServletRequest request, HttpServletResponse response){
//		   PrintWriter out = null;
//		   try {
//			   BufferedReader br = request.getReader();
//			   String line;
//			   line = br.readLine();
//			   response.setStatus(201);
//			  // Set response content type
//			   response.setContentType("text/html");
//			
//			  // Actual logic goes here.
//			out = response.getWriter();
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//			  out.println("<h1>" + message + "</h1>");
//	   }
	   
	   public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException{
		   String nombre = request.getParameter("username");
		   String pass = request.getParameter("password");
		   String[] technologies = request.getParameterValues("technologies");
		   PrintWriter out = response.getWriter();
		   out.println("<h1>USERNAME: " + nombre + "</h1>");
		   out.println("<h1>PASSWORD: " + pass + "</h1>");
		   int i=1;
		   for(String strTech:technologies) {
			   out.println("<h1>Tech "+i+": " + strTech + "</h1>");
			   i++;
		   }
	   }

	   public void destroy() {
	      // do nothing.
	   }

}
