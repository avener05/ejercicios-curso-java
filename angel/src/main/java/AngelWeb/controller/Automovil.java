package AngelWeb.controller;

public class Automovil {
	private String modelo;
	private String placa;
	private int precio;
	
	public Automovil(String modelo, String placa, int precio) {
		super();
		this.modelo = modelo;
		this.placa = placa;
		this.precio = precio;
	}
	
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String getPlaca() {
		return placa;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public int getPrecio() {
		return precio;
	}
	public void setPrecio(int precio) {
		this.precio = precio;
	}
	
}
