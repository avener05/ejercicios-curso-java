package AngelWeb.controller;

import java.util.Set;

public class AutomovilDAOImpl implements AutomovilInterface{
	private Set<Automovil> db;
	
	public AutomovilDAOImpl(Set<Automovil> db) {
		super();
		this.db = db;
	}

	Set<Automovil> getAll(){
		return db;		
	}
	
	boolean addAutomovil(Automovil a) {
		if(db.add(a)) {
			return true;
		}else {
			return false;
		}
	}
	
	boolean removeAutomovil(Automovil a) {
		if(db.remove(a)) {
			return true;
		}else {
			return false;
		}
	}
}
